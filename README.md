# Puppet workstation LinuxMint 20

Basics stuff to install on a fresh workstation with Puppet Apply for **LinuxMint 20**

For the very first run, as user **root**, do:

```shell
apt install git
mkdir -p /root/.puppetlabs/etc/code/modules/
cd /root/.puppetlabs/etc/code/modules/
git clone https://gitlab.com/taqamaqa-puppet/puppet-workstation-linuxmint-20.git
/root/.puppetlabs/etc/code/modules/puppet-workstation-linuxmint-20/files/puppet_workstation.sh
```

If this puppet code is modified, and we want to update a workstation:

1. Get to the parent directory of this repos
1. Then run 

```shell
sudo /opt/puppetlabs/bin/puppet apply --modulepath=/root/.puppetlabs/etc/code/modules/ -e 'include puppet_workstation'
```
