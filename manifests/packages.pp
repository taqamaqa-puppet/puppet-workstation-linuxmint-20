class puppet_workstation::packages {

  # Packages are defined for Ubuntu 18.04

  # Packages from distro
  $package_list = [
    'atop',
    'bash-completion',
    'borgbackup',
    'ccze',
    'curl',
    'chromium-browser',
    'firefox',
    'hunspell',
    'hunspell-fr-comprehensive',
    'thunderbird',
    'vim',
    'whois',
    'zsh'
  ]
  $package_list_dev = [
    'git',
    'httpie',
    'maven',
    'openjdk-8-jdk',
  ]

  # Packages from PPA
  $package_list_from_PPA = [
    'safeeyes',
    'libreoffice',
  ]

  package { $package_list:
    ensure => 'present'
  }
  package { $package_list_dev:
    ensure => 'present'
  }

  Class['apt::update']
  ->
  package { $package_list_from_PPA:
    ensure => 'present'
  }


}
