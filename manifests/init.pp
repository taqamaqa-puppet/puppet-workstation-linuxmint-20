class puppet_workstation {

  include puppet_workstation::ppa
  include puppet_workstation::packages
  include puppet_workstation::aliases

}